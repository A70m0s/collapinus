header = """
<!doctype html>
<html lang="fr"> 
<head>
    <meta charset="utf-8">
    <title>Collapinus</title>
    <meta name="description" content="My content">
    <meta name="author" content="a70m0s">
    <link rel="stylesheet" href="styles.css">
</head>

<body>

"""

class Main():

    def __init__(self):
        self.l_variations = {'nom' : ['us', 'a', 'um', 'is', 'e', 'u', 'es'], 'nom_p' : [],
                            'voc' : ['e', 'a', 'um', 'is', 'e', 'u', 'es'], 'voc_p' : [],
                            'acc' : ['um', 'am', 'em', 'um', 'u', 'em'], 'acc_p' : [],
                            'gen' : ['i', 'ae', 'i', 'is', 'us', 'ei'], 'gen_p' : [],
                            'dat' : ['o', 'ae', 'o', 'i', 'ui', 'ei'], 'dat_p' : [],
                            'abl' : ['o', 'a', 'o', 'e', 'i', 'u'], 'abl_p' : []}
        self.txt_w = []
        self.txt_colors = ['blue', 'grey', 'orange', 'green', 'purple', 'black']
        self.html = open('text.html', 'w')
        self.html_a = open('text.html', 'a')

    def txt_normalizing(self, u_txt):
        txt = ''
        for char in u_txt :
            if char in '.,!?"()[]:;':
                char = ' '
            elif char == 'é' or char == 'è' or char == 'ê' or char == 'ë':
                char = 'e'
            elif char == 'à' or char == 'â' or char == 'ä':
                char = 'a'
            elif char == 'î' or char == 'ï':
                char = 'i'
            elif char == 'ô' or char == 'ö' or char == 'õ':
                char = 'o'
            elif char == 'ù' or char == 'ü' or char == 'û':
                char = 'u'
            elif char == 'ÿ':
                char = 'y'
            txt += char
        txt_f = txt.split()
        return txt_f

    def txt_colorizing(self):
        w = 0
        self.html.write(header)
        u_txt = input("Some text : ")
        txt = self.txt_normalizing(u_txt)
        for word in txt :
            if len(word) < 2 :
                self.html_a.write(word)
                w += 1
            else :
                variation = word[-2] + word[-1]
                variation1 = word[-1]

            if variation in self.l_variations['nom'] :
                self.html_a.write("<span class='nom'>{}</span>".format(word + ' '))
                w += 1
            elif variation1 in self.l_variations['nom'] :
                self.html_a.write("<span class='nom'>{}</span>".format(word + ' '))
                w += 1

            if variation in self.l_variations['voc'] :
                self.html_a.write("<span class='voc'>{}</span>".format(word + ' '))
                w += 1
            elif variation1 in self.l_variations['voc'] :
                self.html_a.write("<span class='voc'>{}</span>".format(word + ' '))
                w += 1

            if variation in self.l_variations['acc'] :
                self.html_a.write("<span class='acc'>{}</span>".format(word + ' '))
                w += 1
            elif variation1 in self.l_variations['acc'] :
                self.html_a.write("<span class='acc'>{}</span>".format(word + ' '))
                w += 1

            if variation in self.l_variations['gen'] :
                self.html_a.write("<span class='gen'>{}</span>".format(word + ' '))
                w += 1
            elif variation1 in self.l_variations['gen'] :
                self.html_a.write("<span class='gen'>{}</span>".format(word + ' '))
                w += 1

            if variation in self.l_variations['dat'] :
                self.html_a.write("<span class='dat'>{}</span>".format(word + ' '))
                w += 1
            elif variation1 in self.l_variations['dat'] :
                self.html_a.write("<span class='dat'>{}</span>".format(word + ' '))
                w += 1

            if variation in self.l_variations['abl'] :
                self.html_a.write("<span class='abl'>{}</span>".format(word + ' '))
                w += 1
            elif variation1 in self.l_variations['abl'] :
                self.html_a.write("<span class='abl'>{}</span>".format(word + ' '))
                w += 1

            if w > 10 :
                self.html_a.write('<br>')
                w=0
        self.html.close()
        self.html_a.write('</body>')
        self.html_a.close()

if __name__ == "__main__":
    toto = Main()
    toto.txt_colorizing()
