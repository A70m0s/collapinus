# Collapinus

A simple way to identify variations in a latin text.

----

![What it does](screenshot.png)

# What it will do

- [ ] Tell you the correct latin variation for each word
- [ ] Write for you a nice webpage with the result